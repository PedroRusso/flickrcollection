import XCTest
import OHHTTPStubs
@testable import FlickrCollection

class FlickrCollectionTests: XCTestCase {    
    func testGetImageSuccess() {
        let json : NSDictionary = [Key.JSON.Response.stat: "OK"]
        
        let matcher: OHHTTPStubsTestBlock = { (request) -> Bool in
            return true
        }
        
        stub(condition: matcher) { _ in
            return OHHTTPStubsResponse(jsonObject: json, statusCode: 200, headers: nil)
        }
        
        let expection = expectation(description: "getImage")
        
        var response : String?
        var error : Error?
        
        APIClient.shared.getImages(tag: "house", page: 1, onSuccess: {result in
            response = result[Key.JSON.Response.stat] as? String
            expection.fulfill()
        }, onError: {myerror in
            error = myerror
        })
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        XCTAssertNotNil(response, "Response should not be nil")
        XCTAssertEqual(response!, "OK", "Response status should be OK")
        XCTAssertNil(error, "error should be nil")
    }
    
    func testGetImageFailure() {
        let json : NSDictionary = [Key.JSON.Response.stat: "fail",
                                   Key.JSON.APIError.code: 100]
        
        let matcher: OHHTTPStubsTestBlock = { (request) -> Bool in
            return true
        }
        
        stub(condition: matcher) { _ in
            return OHHTTPStubsResponse(jsonObject: json, statusCode: 200, headers: nil)
        }
        
        let expection = expectation(description: "getImage")
        
        var response : String?
        var error : APIError?
        
        APIClient.shared.getImages(tag: "house", page: 1, onSuccess: {result in
            response = result[Key.JSON.Response.stat] as? String
        }, onError: {myerror in
            error = myerror as? APIError
            expection.fulfill()
        })
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        XCTAssertNil(response, "Response should be nil")
        XCTAssertNotNil(error, "error should be nil")
        XCTAssertEqual(error?.code, 100, "Response status should be fail")
        
    }
}
