import UIKit

class PhotoListEntry{
    var id: String
    var title: String
    var server: String
    var imagesSizes: [PhotoSize]
    var thumbnail: UIImage?
    
    init (id: String, title: String, server: String) {
        self.id = id
        self.title = title
        self.server = server
        self.imagesSizes = [PhotoSize]()
    }
    
    convenience init(adapter: PhotoListEntryAdapter) {
        self.init(id: adapter.id!, title: adapter.title!, server: adapter.title!)
    }
    
    func setImage(_ image: UIImage) {
        self.thumbnail = image
    }
}


