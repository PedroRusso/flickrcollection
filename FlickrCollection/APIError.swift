import Foundation

class APIError: Error {
    private(set) var code: Int?
    private(set) var message: String?
    
    init(code: Int, message: String) {
        self.code = code
        self.message = message
    }
    
    convenience init(json: NSDictionary) {
        let message = json[Key.JSON.APIError.message] as? String ?? ""
        let code = json[Key.JSON.APIError.code] as? Int ?? 0
        
        self.init(code: code, message: message)
    }
    
    func isInvalidAPIKey() -> Bool {
        return code == 100
    }
    
    func isServiceUnavailable() -> Bool {
        return code == 105
    }
    
    func hasWriteOperationFailed() -> Bool {
        return code == 106
    }
    
    func isBadURL() -> Bool {
        return code == 116
    }
    
    func getErrorMessage() -> String {
        if isInvalidAPIKey() {
            return Key.ErrorMessage.invalidAPIKey.localized
        }
        
        if isServiceUnavailable() {
            return Key.ErrorMessage.serviceUnavailable.localized
        }
        
        if hasWriteOperationFailed() {
            return Key.ErrorMessage.writeOperationFailed.localized
        }
        
        if isBadURL() {
            return Key.ErrorMessage.badURL.localized
        }
        
        return "UnknownError"
    }
}
