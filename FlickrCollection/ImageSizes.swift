enum ImageSizes: String {
    case square = "Square"
    case thumbnail = "Thumbnail"
    case large = "Large"
    case original = "Original"
    case largeSquare = "LargeSquare"
}

func getImageSize(_ entryPhotoSizes: [PhotoSize], imageSize: ImageSizes) -> [PhotoSize] {
    return entryPhotoSizes.filter{ size in size.label.replacingOccurrences(of: " ", with: "") == imageSize.rawValue}
}
