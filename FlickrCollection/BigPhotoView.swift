import Foundation
import UIKit

class BigPhotoView: UIImageView {
    fileprivate var originalPosition: CGRect?
    fileprivate let introAnimationDuration: TimeInterval = 0.6
    
    func setupAndAnimate(photoURL: String, viewToAddInto view: UIView) {
        view.addSubview(self)
        view.bringSubview(toFront: self)
        self.contentMode = .scaleAspectFit
        self.alpha = 0.0
        
        originalPosition = self.frame
        UIView.addActivityIndicator(to: self)
        
        initialAnimation()
        
        self.sd_setImage(with: URL(string: photoURL), completed: {(_, _, _, _) in
            UIView.clearActivityIndicator(from: self)
        })
        
        self.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(clearView(sender:)))
        self.addGestureRecognizer(tapGesture)
    }
    
    fileprivate func initialAnimation(){
        UIView.animate(withDuration: introAnimationDuration, animations: {
            let screenSize = UIScreen.main.bounds
            self.frame.size.width = screenSize.width
            self.frame.size.height = screenSize.height
            self.frame.origin = screenSize.origin
            self.alpha = 1
        })
    }
    
    @objc func clearView(sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: introAnimationDuration, animations: {[weak self] in
            self?.frame = self?.originalPosition ?? CGRect.zero
            self?.alpha = 0
        }) {[weak self] (finished) in
            self?.removeFromSuperview()
            self?.removeGestureRecognizer(sender)
        }
    }
}

