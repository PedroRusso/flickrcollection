import Foundation

class PhotoSize{
    var label: String
    var width: Int32
    var height: Int32
    var source: String
    
    init(label: String, width: Int32, height: Int32, source: String){
        self.label = label
        self.width = width
        self.height = height
        self.source = source
    }
    
    convenience init(adapter: PhotoSizeAdapter) {
        self.init(label: adapter.label!, width: adapter.width!, height: adapter.height!, source: adapter.source! as String)
    }
}
