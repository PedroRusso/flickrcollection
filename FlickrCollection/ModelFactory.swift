import Foundation

final class ModelFactory {
    static func getPhotoSizes(fromJSON json: NSDictionary) -> [PhotoSize] {
        let sizes = ((json["sizes"] as? NSDictionary)?["size"] as? NSArray)?.map { sizeJSON in
            PhotoSize(adapter: PhotoSizeAdapter(json: sizeJSON as! NSDictionary))
        }
        return sizes ?? []
    }
    
    static func getPhotoEntries(fromJSON json: NSDictionary) -> [PhotoListEntry] {
        let entries = ((json["photos"] as? NSDictionary)?["photo"] as? NSArray)?.map {entry in
            PhotoListEntry(adapter: PhotoListEntryAdapter(json: entry as! NSDictionary))
        }
        return entries ?? []
    }
}

