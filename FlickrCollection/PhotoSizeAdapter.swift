import UIKit

class PhotoSizeAdapter {
    var label: String?
    var width: Int32?
    var height: Int32?
    var source: String?
    
    
    init(json: NSDictionary){
        photoSizeFromJSON(json)
    }
    
    func photoSizeFromJSON(_ json: NSDictionary) {
        label = json[Key.JSON.PhotoSize.label] as? String
        width = (json[Key.JSON.PhotoSize.width] as AnyObject).int32Value
        height = (json[Key.JSON.PhotoSize.height] as AnyObject).int32Value
        source = json[Key.JSON.PhotoSize.source] as? String
    }
}
