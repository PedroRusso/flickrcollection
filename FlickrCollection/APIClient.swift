import Foundation
import Alamofire

final class APIClient {
    
    fileprivate static let configName   = "APIConfig"

    fileprivate let FORMAT_TYPE         = "json"
    fileprivate let JSON_CALLBACK       = "1"
    
    fileprivate let APIKEY: String
    fileprivate let baseAPIURL: String
    
    private let MAX_RETRIES = 5
    static let shared = APIClient()
    
    enum APIRequest {
        case getImages(tag: String, page: Int, onSuccess: onSuccessFuncType, onError: onErrorFuncType)
        case getImageSizes(photoID: String, onSuccess: onSuccessFuncType, onError: onErrorFuncType)
    }
    
    private static var Manager: Alamofire.SessionManager = {
        
        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            Key.URLs.localhost: .disableEvaluation
        ]
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        return manager
    }()
    
    fileprivate init() {
        APIKEY = APIClient.apiKeyFromConfigFile()
        baseAPIURL = APIClient.apiURLFromConfigFile()
    }
    
    func getImages(tag: String, page:Int, onSuccess: @escaping onSuccessFuncType, onError: @escaping onErrorFuncType) {
        self.getImages(tag: tag, page: page, onSuccess: onSuccess, onError: onError, attempt: 1)
    }
    
    func getImageSizes(photoID: String, onSuccess: @escaping onSuccessFuncType, onError: @escaping onErrorFuncType) {
        self.getImageSizes(photoID: photoID, onSuccess: onSuccess, onError: onError, attempt: 1)
    }
    
    fileprivate func getImageSizes(photoID: String, onSuccess: @escaping onSuccessFuncType, onError: @escaping onErrorFuncType, attempt: Int) {
        let parameters: Dictionary = [Key.JSON.Parameters.method : "flickr.photos.getSizes",
                                      Key.JSON.Parameters.apiKey : APIKEY,
                                      Key.JSON.Parameters.photoID: photoID,
                                      Key.JSON.Parameters.format : FORMAT_TYPE,
                                      Key.JSON.Parameters.nojsoncallback : JSON_CALLBACK]
        
        apiRequest(method: .get, parameters: parameters, onCompletion: onSuccess, onError: {[weak self] error in
            if (self?.isRecoverableException (error! as NSError) ?? false)  && attempt < (self?.MAX_RETRIES) ?? 0{
                self?.retryRequest(attempt: attempt, request: .getImageSizes(photoID: photoID, onSuccess: onSuccess, onError: onError))
                return
            }
            DispatchQueue.main.async {
                onError(error)
            }
        })
    }    
    
    fileprivate func getImages(tag: String, page:Int, onSuccess: @escaping onSuccessFuncType, onError: @escaping onErrorFuncType, attempt: Int) {
        let parameters: Dictionary = [Key.JSON.Parameters.method: "flickr.photos.search",
                          Key.JSON.Parameters.apiKey : APIKEY,
                          Key.JSON.Parameters.tags : tag,
                          Key.JSON.Parameters.format : FORMAT_TYPE,
                          Key.JSON.Parameters.nojsoncallback : JSON_CALLBACK,
                          Key.JSON.Parameters.page : "\(page)"]
        
        apiRequest(method: .get, parameters: parameters, onCompletion: onSuccess, onError: {[weak self] error in
            if (self?.isRecoverableException (error! as NSError) ?? false)  && attempt < (self?.MAX_RETRIES) ?? 0{
                self?.retryRequest(attempt: attempt, request: .getImages(tag: tag, page: page, onSuccess: onSuccess, onError: onError))
                return
            }
            DispatchQueue.main.async {
                onError(error)
            }
        })
    }
    
    fileprivate func apiRequest(method: HTTPMethod, parameters: [String:Any], onCompletion: @escaping onSuccessFuncType, onError: @escaping onErrorFuncType)
    {
        let fullURL = baseAPIURL
        
        APIClient.Manager.request(fullURL, method: method, parameters: parameters).responseJSON {
            response in switch response.result {
            case .success(let json):
                guard let response = json as? NSDictionary else {
                    return
                }
                
                guard let stat = response[Key.JSON.Response.stat] as? String,
                    stat != Key.JSON.Response.fail else {
                        let error = APIError(json: response)
                        DispatchQueue.main.async {
                            onError(error)
                        }
                        return
                }
                
                onCompletion(response)
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    fileprivate func retryRequest(attempt: Int, request: APIRequest) {
        let currentAttempt = attempt + 1
        print("\(Key.Message.recoverableException) \(currentAttempt)")
        
        switch request{
        case .getImages(let tag, let page, let onSuccess, let onError) :
            getImages(tag: tag, page: page, onSuccess: onSuccess, onError: onError, attempt: currentAttempt)
            return
        case .getImageSizes(let photoID, let onSuccess, let onError) :
            getImageSizes(photoID: photoID, onSuccess: onSuccess, onError: onError, attempt: currentAttempt)
        }
    }
    
    fileprivate func isRecoverableException(_ e: NSError) -> Bool {
        return (e._code == NSURLErrorCannotFindHost || e._code == NSURLErrorCannotConnectToHost || e._code == NSURLErrorTimedOut)
    }
}

extension APIClient {
    fileprivate static func apiKeyFromConfigFile() -> String {
        var myDict: NSDictionary?
        
        if let path = Bundle.main.path(forResource: configName, ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
        } else {
            print("Error: Invalid path to \(configName) file.")
            return ""
        }
        
        guard let _ = myDict,
            let apiKey = myDict?["apiKey"] else{
                print("Error: APIKey not found.")
                return ""
        }
        return (apiKey as? String) ?? ""
    }
    
    fileprivate static func apiURLFromConfigFile() -> String {
        var myDict: NSDictionary?
        
        if let path = Bundle.main.path(forResource: configName, ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
        } else {
            print("Error: Invalid path to \(configName) file.")
            return ""
        }
        
        guard let _ = myDict,
            let apiURL = myDict?["apiURL"] else{
                print("Error: APIURL not found.")
                return ""
        }
        return (apiURL as? String) ?? ""
    }
    
}
