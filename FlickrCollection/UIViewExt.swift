import UIKit

extension UIView {
    static func addActivityIndicator(to parentView: UIView?) {
        guard let parentView = parentView else { return }
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        parentView.backgroundColor = UIColor.white

        activityIndicator.isHidden = false
        activityIndicator.translatesAutoresizingMaskIntoConstraints = true 
        parentView.addSubview(activityIndicator)
        
        activityIndicator.center = CGPoint(x: parentView.bounds.midX, y: parentView.bounds.midY)
        activityIndicator.autoresizingMask = [
            .flexibleLeftMargin,
            .flexibleRightMargin,
            .flexibleTopMargin,
            .flexibleBottomMargin
        ]
        
        activityIndicator.startAnimating()
    }
    
    static func clearActivityIndicator(from parentView: UIView?) {
        let myViews = parentView?.subviews.filter{$0 is UIActivityIndicatorView}
        guard let _ = myViews else { return }
        for view in myViews! {
            view.removeFromSuperview()
        }
    }
}
