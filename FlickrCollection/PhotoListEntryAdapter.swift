import UIKit

class PhotoListEntryAdapter {
    var id: String?
    var title: String?
    var server: String?
    var image: UIImage?

    
    init(json: NSDictionary){
        photoFromJSON(json)
    }
    
    func photoFromJSON(_ json: NSDictionary) {
        id = json[Key.JSON.Photo.id] as? String
        title = json[Key.JSON.Photo.title] as? String
        server = json[Key.JSON.Photo.server] as? String
    }
}
