import UIKit

func presentError(_ error: NSError?) -> UIAlertController {
    let alertController = UIAlertController(title: Key.Alert.errorTitle, message: error?.description, preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: Key.Alert.buttonTitleOk, style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
        alertController.dismiss(animated: true, completion: nil)
    }))
    return alertController
}

func presentError(_ message: String) -> UIAlertController {
    let alertController = UIAlertController(title: Key.Alert.errorTitle, message: message, preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: Key.Alert.buttonTitleOk, style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
        alertController.dismiss(animated: true, completion: nil)
    }))
    return alertController
}

func presentNotificationAlert(_ message: String) -> UIAlertController {
    return presentError(message)
}

func presentError(_ message: String, title: String, action: (()->())? = nil) -> UIAlertController{
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: Key.Alert.buttonTitleOk, style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
        action?()
    }))
    return alertController
}

func presentNotificationAlert(_ message: String, title: String, action: (()->())?) -> UIAlertController {
    return presentError(message, title: title, action: action)
}

func presentAlert(_ message: String, title: String, action: (()->())? = nil) -> UIAlertController{
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    alertController.addAction(UIAlertAction(title: Key.Alert.buttonTitleCancel, style: UIAlertActionStyle.default, handler: nil))
    alertController.addAction(UIAlertAction(title: Key.Alert.buttonTitleOk, style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
        action?()
    }))
    
    return alertController
}

func presentAlert(_ message: String, title: String, okButtonTitle: String, cancelButtonTitle:String, okAction: (()->())?, cancelAction: (()->())?) -> UIAlertController {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    alertController.addAction(UIAlertAction(title: cancelButtonTitle, style: UIAlertActionStyle.default, handler: { (ACTION :UIAlertAction!)in
        cancelAction?()
    }))
    alertController.addAction(UIAlertAction(title: okButtonTitle, style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
        okAction?()
    }))
    
    return alertController
}
