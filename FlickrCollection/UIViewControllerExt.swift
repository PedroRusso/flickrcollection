import UIKit

extension UIViewController {
    func showError(error: Error?) {
        var alert: UIAlertController
        let myError = error as? APIError
        
        if myError == nil {
            alert = presentError(error?.localizedDescription ?? "")
        }else {
            alert = presentError(myError!.getErrorMessage())
        }
        self.present(alert, animated: true, completion: nil)
    }
}
