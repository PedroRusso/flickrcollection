import UIKit
import SDWebImage
import AMScrollingNavbar

final class PhotoCollectionViewController: UICollectionViewController {

    fileprivate let reuseIdentifier = "flickrCollectionCell"
    fileprivate let itemsPerRow: CGFloat = 2
    fileprivate let sectionInsets = UIEdgeInsets(top: 30.0, left: 10.0, bottom: 30.0, right: 10.0)
    fileprivate var currentPage = 1
    fileprivate var currentSearch = "kitten"
    
    fileprivate var searches = [PhotoListEntry]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.addActivityIndicator(to: collectionView)
        fetchImageEntries(tag: currentSearch, page: currentPage, activityIndicator: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let navigationController = navigationController as? ScrollingNavigationController {
            navigationController.followScrollView(collectionView!, delay: 50.0)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if let navigationController = navigationController as? ScrollingNavigationController {
            navigationController.stopFollowingScrollView()
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        guard let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayout.invalidateLayout()
    }

    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {        
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoCollectionViewCell
        
        let initialPoint = cell.superview?.convert(cell.frame.origin, to: nil) ?? CGPoint.zero
        let imageView = BigPhotoView(frame: CGRect(origin: initialPoint, size: cell.imageView.frame.size))
        
        let photoEntry = searches[indexPath.row]
        let largePhotoSize = getImageSize(photoEntry.imagesSizes, imageSize: .large)
        
        if largePhotoSize.count > 0 {
            let size = largePhotoSize.first!
            imageView.setupAndAnimate(photoURL: size.source, viewToAddInto: self.view)
        }
    }
}

extension PhotoCollectionViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text != nil && textField.text!.count > 0 {
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            textField.addSubview(activityIndicator)
            activityIndicator.frame = textField.bounds
            activityIndicator.startAnimating()
            
            currentSearch = textField.text!
            currentPage = 1
            searches = []
            
            fetchImageEntries(tag: currentSearch, page: currentPage, activityIndicator: activityIndicator)
            
            textField.text = ""
        }
        textField.resignFirstResponder()
        return true
    }
}

extension PhotoCollectionViewController {
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searches.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PhotoCollectionViewCell

        let photoEntry = searches[indexPath.row]
        cell.imageView.image = nil
        
        // Check for photoSize info to prevent re-hitting the network
        if photoEntry.imagesSizes.count > 0 {
            UIView.clearActivityIndicator(from: cell)
            return fetchImage(photoEntry: photoEntry, cell: cell)
        }
        
        UIView.addActivityIndicator(to: cell)
        
        APIClient.shared.getImageSizes(photoID: photoEntry.id, onSuccess: {[weak self] json in

            guard let strongSelf = self else {
                return
            }
            
            DispatchQueue.global(qos: .userInitiated).async {
                let sizes = ModelFactory.getPhotoSizes(fromJSON: json)
                photoEntry.imagesSizes = sizes
                let _ = strongSelf.fetchImage(photoEntry: photoEntry, cell: cell)
            }

            // Check for page update
            let currentPage = (strongSelf.currentPage)
            if indexPath.row == ((currentPage * 100) - 20) {
                strongSelf.currentPage = currentPage + 1
                strongSelf.fetchImageEntries(tag: (strongSelf.currentSearch), page: (strongSelf.currentPage), activityIndicator: nil)
            }
            }, onError: {[weak self] error in

                guard let strongSelf = self else {
                    return
                }
                strongSelf.showError(error: error)
        })
        
        return cell
    }
    
    fileprivate func fetchImage(photoEntry: PhotoListEntry, cell: PhotoCollectionViewCell) -> PhotoCollectionViewCell {
        let thumbEntry = getImageSize(photoEntry.imagesSizes, imageSize: .largeSquare)
        
        if thumbEntry.count > 0 {
            let size = thumbEntry.first!
            cell.imageView.sd_setImage(with: URL(string: size.source), completed: {(_, _, _, _) in
                UIView.clearActivityIndicator(from: cell)
            })
        }
        return cell
    }
    
    fileprivate func fetchImageEntries(tag: String, page: Int, activityIndicator: UIActivityIndicatorView?) {
        APIClient.shared.getImages(tag: tag,
                                   page: page,
                                   onSuccess: {[weak self] json in

                                    guard let strongSelf = self else {
                                        return
                                    }

                                    let entries = ModelFactory.getPhotoEntries(fromJSON: json)
                                    strongSelf.searches.append(contentsOf: entries)
                                    
                                    UIView.clearActivityIndicator(from: strongSelf.collectionView)
                                    strongSelf.collectionView?.reloadData()
                                    
                                    if strongSelf.currentPage == 1 && entries.count > 0 {
                                        strongSelf.collectionView?.scrollToItem(at: IndexPath(row: 0, section: 0), at: UICollectionViewScrollPosition.top, animated: false)
                                    }
                                    
                                    activityIndicator?.removeFromSuperview()
            }, onError: {[weak self] error in

                guard let strongSelf = self else {
                    return
                }

                strongSelf.showError(error: error)
        })
    }
}

extension PhotoCollectionViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        collectionView.frame = UIScreen.main.bounds
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
