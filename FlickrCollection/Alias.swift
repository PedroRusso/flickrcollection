import Foundation

typealias onSuccessFuncType = (NSDictionary) -> ()
typealias onErrorFuncType = (Error?) -> ()
