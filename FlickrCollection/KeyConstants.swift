struct Key {
    
    static let DeviceType = "iOS"
    
    struct URLs {
        static let baseAPIURL = "https://localhost:3443/api/v2.0/"
        static let localhost = "localhost"
    }
        
    struct JSON {
        
        struct Photo {
            static let id = "id"
            static let title = "title"
            static let server = "server"
        }
        
        struct PhotoSize {
            static let label = "label"
            static let width = "width"
            static let height = "height"
            static let source = "source"
        }
        
        struct Parameters {
            static let method = "method"
            static let apiKey = "api_key"
            static let tags = "tags"
            static let format = "format"
            static let nojsoncallback = "nojsoncallback"
            static let page = "page"
            static let photoID = "photo_id"
        }
        
        struct APIError {
            static let code = "code"
            static let message = "message"
        }
        
        struct Response {
            static let stat = "stat"
            static let fail = "fail"
        }
    }
    
    struct ErrorMessage {
        static let invalidAPIKey = "invalidAPIKeyMessage"
        static let serviceUnavailable = "serviceUnavailableMessage"
        static let writeOperationFailed = "writeOperationFailedMessage"
        static let badURL = "badURLMessage"
    }
    
    struct Alert {
        static let errorTitle = "Error"
        static let buttonTitleOk = "Ok"
        static let buttonTitleCancel = "Cancel"
    }
    
    struct Message {
        static let recoverableException = "Recoverable exception. Attempt:"
    }
    
    
}
